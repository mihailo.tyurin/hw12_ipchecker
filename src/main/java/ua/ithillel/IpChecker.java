package ua.ithillel;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;

public class IpChecker implements Runnable {
    private final String ip;

    public IpChecker(String ip) {
        this.ip = ip;
    }

    @Override
    public void run() {
        String fileName;
        try {
            if (sendPingRequest(ip)) {
                fileName = "available.txt";
            } else {
                fileName = "unavailable.txt";
            }
            writer(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private boolean sendPingRequest(String ipAddress) throws IOException {
        InetAddress checker = InetAddress.getByName(ipAddress);
        System.out.println("Sending Ping Request to " + ipAddress);
        return checker.isReachable(2500);
    }

    private void writer(String fileName) throws IOException {
        synchronized (fileName) {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true))) {
                bw.write(ip + "   -->" + Thread.currentThread() + "\n");
                bw.flush();
            }
        }
    }
}
