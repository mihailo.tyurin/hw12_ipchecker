package ua.ithillel;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(4);

        try (BufferedReader br = new BufferedReader(new FileReader("ip address.txt"))) {
            String ip;
            while ((ip = br.readLine()) != null) {
                executor.execute(new IpChecker(ip));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        executor.shutdown();
    }
}
